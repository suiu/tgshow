package com.splatform.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by sunfeihu on 2017/4/22.
 */
@Controller
public class BaiduBridgeController {

    @GetMapping("/birdge")
    public String birdge() {
        return "birdge";
    }
}
